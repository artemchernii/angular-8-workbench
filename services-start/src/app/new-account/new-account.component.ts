import { Component } from '@angular/core';

import { AccountServiceService } from './../account-service.service';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css'],
  providers: []
})
export class NewAccountComponent {
  constructor(private accountService: AccountServiceService) {
    this.accountService.statusUpdated.subscribe((status: string) => {
      alert(`New Status '${status}'`);
    });
  }

  onCreateAccount(accountName: string, accountStatus: string) {
    this.accountService.addAccount(accountName, accountStatus);
    this.accountService.consoleLogStatus(accountStatus);
  }
}

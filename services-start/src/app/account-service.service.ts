import { LoggingService } from './logging.service';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AccountServiceService {
  accounts = [
    {
      name: 'Master Account',
      status: 'active'
    },
    {
      name: 'Testaccount',
      status: 'inactive'
    },
    {
      name: 'Hidden Account',
      status: 'unknown'
    }
  ];
  statusUpdated = new EventEmitter<string>();

  constructor(private loggingService: LoggingService) {}

  addAccount(name: string, status: string) {
    this.accounts.push({ name: name, status: status });
  }
  updateStatus(id: number, status: string) {
    this.accounts[id].status = status;
  }
  consoleLogStatus(status: string) {
    this.loggingService.logStatusChange(status);
  }
  simpleConsoleItem(item: string) {
    this.loggingService.simpleConsole(item);
  }
}

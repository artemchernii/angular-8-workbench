import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CounterService {
  public inactiveTimes = 0;
  public activeTimes = 0;
  constructor() {}

  onActive() {
    this.activeTimes++;
  }
  onInactive() {
    this.inactiveTimes++;
  }
}

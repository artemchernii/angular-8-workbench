import { CounterService } from './../shared/counter.service';
import { UserService } from './../shared/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.css']
})
export class ActiveUsersComponent implements OnInit {
  public users = [];
  constructor(
    private userService: UserService,
    private counterService: CounterService
  ) {}
  public activeTimes = this.counterService.activeTimes;
  ngOnInit() {
    this.users = this.userService.activeUsers;
  }
  onSetToInactive(id: number) {
    this.userService.onSetToInactive(id);
    this.counterService.onActive();
    this.activeTimes = this.counterService.activeTimes;
  }
}

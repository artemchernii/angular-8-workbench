import { CounterService } from './../shared/counter.service';
import { UserService } from './../shared/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inactive-users',
  templateUrl: './inactive-users.component.html',
  styleUrls: ['./inactive-users.component.css']
})
export class InactiveUsersComponent implements OnInit {
  public users = [];
  public inactiveTimes = this.counterService.inactiveTimes;
  constructor(
    private userService: UserService,
    private counterService: CounterService
  ) {}
  ngOnInit() {
    this.users = this.userService.inactiveUsers;
  }
  onSetToActive(id: number) {
    this.userService.onSetToActive(id);
    this.counterService.onInactive();
    this.inactiveTimes = this.counterService.inactiveTimes;
  }
}

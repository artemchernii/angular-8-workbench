import { ServerModule } from './models/server/server.module';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public serverElements: ServerModule[] = [
    {
      type: 'server',
      name: 'Big Black Server',
      content: 'Stories about content'
    },
    {
      type: 'blueprint',
      name: 'Big Blue Server',
      content: 'Stories about another content'
    }
  ];

  constructor() {}

  ngOnInit() {}
  onServerAdded(serverData: { serverName: string; serverContent: string }) {
    this.serverElements.push({
      type: 'server',
      name: serverData.serverName,
      content: serverData.serverContent
    });
  }
  onBlueprintAdded(blueprintData: {
    blueprintName: string;
    blueprintContent: string;
  }) {
    this.serverElements.push({
      type: 'blueprint',
      name: blueprintData.blueprintName,
      content: blueprintData.blueprintContent
    });
  }
}

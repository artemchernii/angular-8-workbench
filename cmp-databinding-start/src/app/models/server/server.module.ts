import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [CommonModule]
})
export class ServerModule {
  constructor(
    public type: string,
    public name: string,
    public content: string
  ) {}
}

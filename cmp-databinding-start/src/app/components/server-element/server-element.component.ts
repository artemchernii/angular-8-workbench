import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  OnChanges,
  SimpleChanges,
  DoCheck,
  AfterContentInit,
  ViewChild,
  ElementRef,
  ContentChild
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ServerElementComponent
  implements OnInit, OnChanges, DoCheck, AfterContentInit {
  @Input('servElement') element: {
    type: string;
    name: string;
    content: string;
  };
  @Input() name: string;
  @ViewChild('heading', { static: true }) header: ElementRef;
  constructor() {
    console.log('Constructor');
  }
  @ContentChild('contentParagraph', { static: true }) paragraph: ElementRef;
  ngOnChanges(changes: SimpleChanges) {
    console.log('NgOnChanges');
    // console.log(changes);
  }
  ngOnInit() {
    console.log('NgOnInit');
    // console.log(this.header.nativeElement.textContent);
    console.log(this.paragraph.nativeElement.textContent);
  }
  ngDoCheck(): void {
    console.log('NgDoCheck');
  }
  ngAfterContentInit() {
    console.log('After Content Init');
    // console.log(this.header.nativeElement.textContent);
    console.log(this.paragraph.nativeElement.textContent);
  }
}

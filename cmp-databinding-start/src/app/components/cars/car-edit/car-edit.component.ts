import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'app-car-edit',
  templateUrl: './car-edit.component.html',
  styleUrls: ['./car-edit.component.css']
})
export class CarEditComponent implements OnInit {
  @Output() editCarList = new EventEmitter<{ name: string; image: string }>();
  @ViewChild('nameInput', { static: true }) name: ElementRef;
  public image: string = '';
  constructor() {}

  ngOnInit() {}

  addCartoList() {
    this.editCarList.emit({
      name: this.name.nativeElement.value,
      image: this.image
    });
  }
}

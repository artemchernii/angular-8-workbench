import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
  public cars = [
    {
      name: 'Gle',
      image:
        'https://www.mercedes-benz.com/media/cache/aem_top_stage_2x/content/mbcom/en/mercedes-benz/vehicles/passenger-cars/gle/all-kinds-of-strength/jcr:content/par/topstage/desktopImage_20180912130519'
    },
    {
      name: 'X6',
      image:
        'https://image-cdn.hypb.st/https%3A%2F%2Fhypebeast.com%2Fimage%2F2019%2F07%2Fbmw-x6-2019-2020-update-suv-crossover-m-sport-1.jpg?q=75&w=800&cbr=1&fit=max'
    }
  ];
  constructor() {}

  ngOnInit() {}
  handlerCars(car: any) {
    this.cars.push(car);
  }
}

import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  public gameStep: number = 0;
  public intervalStep: any;
  @Output() intervalFired = new EventEmitter<number>();
  @Output() resetFired = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  onStartGame() {
    this.intervalStep = setInterval(() => {
      this.intervalFired.emit(this.gameStep + 1);
      this.gameStep++;
    }, 1000);
    return this.intervalStep;
  }
  onStopGame() {
    if (this.intervalStep) {
      clearInterval(this.intervalStep);
    }
  }
  onResetGame() {
    if (this.intervalStep) {
      clearInterval(this.intervalStep);
    }
    this.gameStep = 0;
    this.resetFired.emit();
  }
}

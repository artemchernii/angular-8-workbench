import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-outputimage',
  templateUrl: './outputimage.component.html',
  styleUrls: ['./outputimage.component.css']
})
export class OutputimageComponent implements OnInit {
  @Input() inputvalue: string;
  constructor() {}

  ngOnInit() {}
}

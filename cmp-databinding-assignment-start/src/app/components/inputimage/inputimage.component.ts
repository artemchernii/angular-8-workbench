import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-inputimage',
  templateUrl: './inputimage.component.html',
  styleUrls: ['./inputimage.component.css']
})
export class InputimageComponent implements OnInit {
  @Output() inputHandler = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {}
  changeHandler(event: any) {
    this.inputHandler.emit(event);
  }
}

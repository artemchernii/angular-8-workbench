import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  oddNumbers = [];
  evenNumbers = [];
  inputvalue = '';
  onIntervalFired(firedNumber: number) {
    if (firedNumber % 2 === 0) {
      this.evenNumbers.push(firedNumber);
    } else if (firedNumber % 2 !== 0) {
      this.oddNumbers.push(firedNumber);
    }
  }
  onResetFired() {
    this.oddNumbers.splice(0);
    this.evenNumbers.splice(0);
    console.log(this.inputvalue);
  }
  inputHandler(event: any) {
    this.inputvalue = event.target.value;
  }
}
